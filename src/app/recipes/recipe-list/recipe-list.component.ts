import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Recipe } from '../recipes.model';


@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RecipeListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  recipes: Recipe[] = [
    new Recipe('A Test Recipe','Some text', 'http://www.vonbergens.com/wp-content/uploads/2013/06/recipe.gif'),
  	new Recipe('A Test Recipe','Some text', 'http://www.vonbergens.com/wp-content/uploads/2013/06/recipe.gif')

  ];

}
