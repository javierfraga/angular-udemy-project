import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RecipeDetailsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
